function filterCollection(array , name , flag,...args){
let newArr = [];
let arr = Array.from(args)
name = name.split(' ');

function findVal(target, arr) {
	if(arr.length == 1) {
		return target[arr[0]];
	}
	let nextTarget = target[arr[0]];
	arr.shift();
	return findVal(nextTarget, arr);
};

if(flag === false){
	for (let i = 0; i < array.length; i++) {
		for(let j = 0;j < arr.length; j++){
			for(let k = 0; k < name.length;k++){
				if(arr[j].includes('.')){
				let a = arr[j].split('.')		
					if(array[i].hasOwnProperty(a[0])){
						let val = findVal(array[i],a)
						for(let prop in val){
							let valLower = val[prop].toLowerCase()
							if(valLower === name[k].toLowerCase() && !newArr.includes(array[i])){
								newArr.push(array[i])
							}
						}
					}	
				}
				else{
					for(let elem in array[i][arr[j]]){
						let lower = array[i][arr[j]].toLowerCase()
						if(lower === name[k].toLowerCase() && !newArr.includes(array[i])){
		  					newArr.push(array[i])
						}
					}
				}
		  	}
		}
	}
}
else{
	for (let i = 0; i < array.length; i++) {
		let checkArr = []
		for(let j = 0;j < arr.length; j++){
			for(let k = 0; k < name.length;k++){
				if(arr[j].includes('.')){
				let a = arr[j].split('.')		
					if(array[i].hasOwnProperty(a[0])){
						let val = findVal(array[i],a)
						for(let prop in array[i]){
							for(let prop in val){
							if(val[prop].toLowerCase() === name[k].toLowerCase() && !checkArr.includes(name[k])){
								checkArr.push(name[k])
								if(name.length === checkArr.length && !newArr.includes(array[i])){
									newArr.push(array[i])
								}
							}
							}
						}	
					}	
				}
				else{
					for(let prop in array[i]){
						if(array[i][prop] === name[k] && !checkArr.includes(name[k])){
							checkArr.push(name[k])
							if(name.length === checkArr.length && !newArr.includes(array[i])){
								newArr.push(array[i])
							}
						}
					}
				}
		  	}
		}
	}
}
return newArr;
}



let vehicles = [
				{name: "Jho", role: "user", age: 22},
				{Name:'Jho', description: "Toyota", age: 32},
				{location :{description:{name:'user' , description:'en_US'}},  age: 23},
				{location :{description:{name:'en_US' , description:'Toyota'}},  age: 23},
				{name: "en_US", role: "user", age: 22},
				{location :{description:{name:'user' , description:'nodes'}},  age: 23},
				{name: "Toyota", description: "en_US", age: 22},
				{description: "en_US", role: "user", age: 22},
				]
console.log(filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 
	'location.description.name','location.description.description' , 'location.description'))
